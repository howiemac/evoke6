"""
This module implements the TEXT class (a.k.a. TEXT), used for transient storage and manipulation of strings in the special Evoke text format.

O/S Currently assumes that Page.py is in use....

implements self.formatted() and self.summarised()

also implements the alternative self.markdown()
"""

import re

from markdown import Markdown
#from html2text import html2text

from .evolinks import EvoLinkExtension
from .STR import STR
from .INT import INT
from .library import safeint

def markdown(text, req, *a, **k):
    ""
    extensions = k.setdefault('extensions', [])
    extensions.append(EvoLinkExtension())
    md = Markdown(*a, **k)
    md.req = req
    return md.convert(text)

class TEXT(STR):
    """
  Evoke text format handling
  """

    # extra rules for markdown
    image_rule = re.compile(r'(\[IMG )(.*?)(\])')

    def summarised(self, req, chars=250, lines=3, formatted=True):
        " return summary of formatted text "
        if formatted:
            return self.formatted(req, chars, lines)
        # not formatted - ignore "lines"
        return self[:chars]

    def formatted(self, req, chars=0, lines=0):
        "format in HTML via Markdown"
        self.has_more = False
        # get the text to process
        text = self
        if chars:
            self.has_more = chars < len(self)
            text = self[:chars]
        if lines:
            z = text.splitlines()
            textlines = z[:lines]
            text = "\n".join(textlines) + '\n'
            self.has_more = self.has_more or (lines < len(z))

        def subimage(match):
            "render a [IMG (uid|url) attributes?]"
            source = match.groups()[1]
            # print (match.groups())
            if ' ' in source:
                url, atts = source.split(' ', 1)
            else:
                url, atts = source, ''

            # check for a valid uid
            if safeint(url):
                try:
                    img = req.user.Page.get(safeint(url))
                    url = img.thumb_url()
                except:
                    raise
                    pass

            return '<img src="%s" %s />' % (url, atts)

        return markdown(text, req)


#    def to_markdown(self, req):
#        """Render to html using formatter, then use html2text to convert to Markdown"""
#        html = self.formatted(req).replace('<q>','&ldquo;').replace('</q>','&rdquo;')
#        md = html2text(html)
#
#        # convert links to [url caption] rather than [caption](url)
#        linkfix_rule = re.compile(r'(\[)(.*?)(\]\()(.*?)(\))')
#
#        def sublinkfix(match):
#            ""
#            caption = match.groups()[1].strip()
#            url = match.groups()[3].strip()
#            if url.startswith('/') and INT(url[1:]):
#              url=url[1:]
#            return '[%s %s]' % (url,caption)
#
#        text = linkfix_rule.sub(sublinkfix, md)
#        return text

