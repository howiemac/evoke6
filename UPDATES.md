### evoke data optimisation (patch by CJH 16/12/2018 per evoke 7 master)
20/11/2020

data.get - put the dict for an object directly into ob.__dict__  don't cast as Schema class.

data.__getattribute__ - if a value hasn't been cast into Schema, do so before returning.

Page.get - cache override kind classes rather than recreating each tie.


### evoke ratings upgrade:
31/10/2020

was:

2 = love
1 = like
0 = ? (unsure)
-1 = disabled 2
-2 = disabled 1
-3 = disabled 0
-4 = x (don't want)

now:

3 = great
2 = good
1 = ok
0 = ? (unsure)
-1 = x (don't want)
-2 = disabled 3
-3 = disabled 2
-4 = disabled 1
-5 = disabled 0

Implemented and working.

No explicit conversion required, as existing data will convert fairly gracefully: However enabled "heart" items now become diamonds (i.e. one step down) and there will be no heart items (so the heart symbol has been "promoted") until you create some by uprating pages.

Disabled items will be uprated one step - this is a side effect of the half-baked rating correlations used. If this is a problem, do a manual fix to decrement all page ratings lower than -1 by 1.